import numpy
import pylab
import util.FITS
from scipy.ndimage import interpolation
data=util.FITS.Read("imsol.fits")[1]#imsol is 50 arcsec with 3600 pixels.
nwfs=3
fov=5.
wfsRadius=5.
pxlPerArcsec=3600/50.
b=widefieldImageBoundary=8
wfs_n=8
fftsize=wfs_n*2
nFieldX=6
n=fftsize*(nFieldX+1.)/2
ox=1000
oy=1700
for wfs in range(3):
    offsetx=int(ox+wfsRadius*numpy.cos(wfs*360./nwfs*numpy.pi/180.)*pxlPerArcsec)
    offsety=int(oy+wfsRadius*numpy.sin(wfs*360./nwfs*numpy.pi/180.)*pxlPerArcsec)
    npixels=int(3600/50.*fov*2)
    print "Image offset for wfs %d is %d, %d -> %d, %d"%(wfs,offsetx,offsety,offsetx+npixels,offsety+npixels)
    if offsetx<0 or offsety<0 or offsetx+npixels>data.shape[1] or offsety+npixels>data.shape[0]:
        raise Exception("Outside of bounds")
    img=data[offsety:offsety+npixels,offsetx:offsetx+npixels]*10

    img2=interpolation.zoom(img,(n+2*b)/img.shape[0])*100
    print img2.min()
    pylab.subplot(2,3,wfs+1)
    pylab.imshow(img,interpolation="nearest",cmap="gray")
    pylab.subplot(2,3,wfs+1+3)
    pylab.imshow(img2,interpolation="nearest",cmap="gray")
pylab.show()
