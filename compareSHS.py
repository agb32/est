import time
import numpy
import base.aobase
import util.FITS

class Compare(base.aobase.aobase):
    def __init__(self,parent,config,args={},forGUISetup=0,debug=None,idstr=None):
        base.aobase.aobase.__init__(self,parent,config,args,forGUISetup=forGUISetup,debug=debug,idstr=idstr)
        self.nfList=[]
        self.wfList=[]
        self.rms=[]
        self.iter=0
    def generateNext(self):
        if self.iter>2:
            nf=self.parent["nf"].outputData
            wf=self.parent["wf"].outputData
            self.nfList.append(nf.copy())
            self.wfList.append(wf.copy())
            rms=(nf-wf).std()
            if not numpy.isnan(rms):
                self.rms.append(rms)
        self.iter+=1

    # def __del__(self):
    #     print "Finalising"
    #     self.makeBest()
    #     print "Finished"
    def endSim(self):
        print "Finalising"
        self.makeBest(save=1)
        print "Finished"
        
            
    def makeBest(self,save=0):
        """Computes rms, but after scaling.
        Scales every subap of the widefield image to give best fit.
        nf.shape will be niter,50,50,2
        """
        nf=numpy.array(self.nfList)
        wf=numpy.array(self.wfList)
        #scale widefield slopes to best fit (i.e. we assume a reconstructor will take the centroid gain into account).
        s=(wf**2).sum(0)
        wf=wf*(nf*wf).sum(0)/numpy.where(s==0,1,s)
        diff=nf-wf
        std=diff.std(0,ddof=1)
        meanstdx=std[...,0].mean()
        meanstdy=std[...,1].mean()
        meanoffset=numpy.sqrt(std[...,0]**2+std[...,1]**2).mean()
        print meanstdx,meanstdy,meanoffset
        if save:
            tstamp=time.strftime("%y%m%d_%H%M%S")
            idtxt=""
            if self.config.this.simID!="":
                idtxt=" (%s)"%self.config.this.simID
            
            util.FITS.Write(std,"compareStd.fits",writeMode="a",extraHeader=["TSTAMP  = %s"%tstamp,"SIMID   = %s"%idtxt])
            txt="%s %s meanx %g meany %g meanoffset %g meanrms %g\n"%(tstamp,idtxt,meanstdx,meanstdy,meanoffset,numpy.array(self.rms).mean())
            open("compareStd.csv","a").write(txt)
        return std
    def plottable(self,objname="$OBJ"):
        """Return a XML string which contains the commands to be sent
        over a socket to obtain certain data for plotting.  The $OBJ symbol
        will be replaced by the instance name of the object - e.g.
        if scrn=mkscrns.Mkscrns(...) then $OBJ would be replaced by scrn."""
        txt=""
        txt+="""<plot title="rms" cmd="data=numpy.array(%s.rms)" ret="data" type="pylab" when="rpt"/>\n"""%(objname)
        txt+="""<plot title="wf" cmd="data=%s.wfList[-1]" ret="data" type="pylab" when="rpt"/>\n"""%objname
        txt+="""<plot title="nf" cmd="data=%s.nfList[-1]" ret="data" type="pylab" when="rpt"/>\n"""%objname
        return txt
