#Makes a polc matrix:
# dI + gRP
#where d is decay factor, (probably a bit less than 1-g), g is gain, R is the relevant part of the reconstructor control matrix and P is the interation (poke) matrix.
import sys
import time
import numpy
import cmod.openblas as mkl
import util.FITS
pmx=sys.argv[1]
rmx=sys.argv[2]
gain=float(sys.argv[3])
decay=float(sys.argv[4])#typically 0.9*(1-g) or something.
polc=sys.argv[5]#filename.

pmx=util.FITS.Read(pmx)[1]#nact,nsl
rmx=util.FITS.Read(rmx)[1]#nsl,nact
if pmx.shape[0]>pmx.shape[1]:
    pmx=pmx.T
if rmx.shape[0]<rmx.shape[1]:
    rmx=rmx.T
out=numpy.zeros((pmx.shape[0],pmx.shape[0]),numpy.float32)
print "Starting gemm"
t1=time.time()
mkl.gemm(rmx.T,pmx.T,out,gain)
print "gemm done in %g seconds.  Writing to %s"%(time.time()-t1,polc)
out.ravel()[::pmx.shape[0]+1]+=decay
util.FITS.Write(out,polc,doByteSwap=0,extraHeader=["GAIN    = %g"%gain,"DECAY   = %g"%decay])

