"""Tests the new iscrn module.  
Note - this may not be kept up to date with the module."""
import numpy
import science.iscrn
import science.xinterp_dm
import science.wideField
import science.wfscent
import science.tomoRecon
import science.iatmos
import science.science
import base.readConfig
import util.Ctrl
ctrl=util.Ctrl.Ctrl(globals=globals())
iscrn=science.iscrn.iscrn(None,ctrl.config,idstr="allLayers")
nwfs=ctrl.config.getVal("nwfs")
ndm=ctrl.config.getVal("ndm")
iatmosList=[]
dmList=[]
dm2List=[]
wfList=[]
cList=[]#
wfsdict={}

ctrl.initialCommand("for wf in wfList:\n wf.control['cal_source']=1",freq=-1,startiter=0)
ctrl.initialCommand("for wf in wfList:\n wf.control['cal_source']=0",freq=-1,startiter=1)
ctrl.initialCommand("for c in cList:\n c.newCorrRef()\nprint 'Done new corr ref'",freq=-1,startiter=1)
if not "nopoke" in ctrl.userArgList:
    ctrl.doInitialPokeThenRun(startiter=2)
else:
    ctrl.doInitialOpenLoop(startiter=2)
    ctrl.doInitialSciRun(startiter=5)

iatmosList.append(science.iatmos.iatmos({"allLayers":iscrn},ctrl.config,idstr="b"))#science.
for i in range(ndm):
    dm2List.append(science.xinterp_dm.dm(None,ctrl.config,idstr="dmNF%db"%i))#this one for the Narrow Field science.

for i in range(nwfs):#in direction %d
    d={"allLayers":iscrn}
    for j in range(ndm):
        dmList.append(science.xinterp_dm.dm(None,ctrl.config,idstr="dm%d_%d"%(j,i)))#this one (with no phase) for the widefield object (which adds the phase) in a particular direction.
        d["dm%d_%d"%(j,i)]=dmList[-1]
    wfList.append(science.wideField.WideField(d,ctrl.config,idstr="%d"%i))
    cList.append(science.wfscent.wfscent(wfList[-1],ctrl.config,idstr="%dcent"%i))
    wfsdict["%dcent"%i]=cList[-1]
r=science.tomoRecon.recon(wfsdict,ctrl.config,idstr="recon")
p=iatmosList[0]
for i in range(ndm):
    dm2List[i].newParent({"recon":r,"atmos":p},"dmNF%db"%i)
    p=dm2List[i]

for i in range(nwfs):
    for j in range(ndm):
        dmList[i*ndm+j].newParent({"recon":r},"dm%d_%d"%(j,i))

s=science.science.science(dm2List[-1],ctrl.config,idstr="b")
s2=science.science.science(iatmosList[0],ctrl.config,idstr="buncorr")
execOrder=[iscrn]+iatmosList+dm2List+dmList+wfList+cList+[r,s,s2]
ctrl.mainloop(execOrder)

