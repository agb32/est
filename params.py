#Taken from params2.xml
import base.readConfig
base.readConfig.init(globals())
tstep=1/250.#iteration time step.
AOExpTime=40.

wfs_sig=1e6 #wfs flux - but actually not used (taken from the solar images)

#number of phase pupils.
npup=80

#telescope diameter
telDiam=4.
ntel=npup
wfs_nsubx=10#number of subaps
wfs_n=npup/wfs_nsubx
ngsLam=640.#ngs wavelength.
sciLam=1650.#sci wavelength in nm
nlayer=3
decayFactor=0.99#integrator decay.
fov=5.#only used in param file - fov of the wfs.
nFieldX=6#number of fields to evaluate to make the shs image.
widefieldImageBoundary=8# The extra rows/cols added to the psf. This will ideally depend on pixel scale and seeing - i.e. should be equal to likely maximum spot motion
import util.FITS
data=util.FITS.Read("imsol.fits")[1]#imsol is 50 arcsec with 3600 pixels.
if hasattr(this.globals,"offsetx"):
    offsetx=this.globals.offsetx
else:
    offsetx=1000
if hasattr(this.globals,"offsety"):
    offsety=this.globals.offsety
else:
    offsety=100
npixels=int(3600/50.*fov*2)
data=data[offsety:offsety+npixels,offsetx:offsetx+npixels]*10
#Note - for this one above to work, need centroidPower=2.  And
#wide-field image boundary of 8.

from scipy.ndimage import interpolation
b=widefieldImageBoundary
fftsize=wfs_n*2
n=fftsize*(nFieldX+1.)/2
#For a different image per subap use this one:
#data=interpolation.zoom(data,wfs_nsubx*(n+2*b)/data.shape[0])
#For identical image per subap, use this one:
data=interpolation.zoom(data,(n+2*b)/data.shape[0])*100
widefieldImage=numpy.zeros((wfs_nsubx,wfs_nsubx,n+2*b,n+2*b),numpy.float32)
for i in range(wfs_nsubx):
    for j in range(wfs_nsubx):
        widefieldImage[i,j]=data # use this for same image per subap.

studySubap=(3,3)
telSec=0.
import util.tel
spider=None
pupil=util.tel.Pupil(npup,ntel/2,ntel/2*telSec/telDiam,spider=spider)

layerList={"L0-2":["L%d"%x for x in range(nlayer)]}

from scipy.ndimage import interpolation
nimg=int(wfs_n*(nFieldX+1.)/2)
corrPattern=numpy.zeros((wfs_nsubx,wfs_nsubx,nimg*2,nimg*2),"f")
#for i in range(wfs_nsubx):
#    for j in range(wfs_nsubx):
#        interpolation.zoom(widefieldImage[i,j],float(nimg)/widefieldImage.shape[2],output=corrPattern[i,j,nimg//2:nimg//2+nimg,nimg//2:nimg//2+nimg])


import util.guideStar
#create the wfs objects:  First one is for widefield image generation
wfsOverview=util.guideStar.wfsOverview({"a":util.guideStar.NGS("a",wfs_nsubx,0.,0.,npup/wfs_nsubx,sig=wfs_sig,sourcelam=ngsLam,fov=fov,pupil=pupil,spotpsf=numpy.ones((wfs_nsubx,wfs_nsubx,npup/wfs_nsubx*4,npup/wfs_nsubx*4),numpy.float32),floor=5000.),
#and this one is for the image -> slope module.
"acent":util.guideStar.NGS("acent",wfs_nsubx,0.,0.,npup/wfs_nsubx,sig=wfs_sig,sourcelam=ngsLam,fov=fov,pupil=pupil,nimg=nimg,ncen=nimg,correlationCentroiding=1,corrThresh=0.3,corrPattern=corrPattern,cameraImage=1,reconList=["recon"],parabolicFit=0,centroidPower=2.0),
})

import util.sci
sciOverview=util.sci.sciOverview({"b":util.sci.sciInfo("b",0.,0.,pupil,sciLam,calcRMS=1),
"buncorr":util.sci.sciInfo("buncorr",0.,0.,pupil,sciLam,calcRMS=1),})

#Now atmosphere stuff
from util.atmos import geom,layer,source
import util.compare
strList=[0.5,0.3,0.2]
hList=[0.,4000.,10000.]
vList=[4.55,12.61,8.73]
dirList=numpy.arange(10)*36
d={}
for i in range(nlayer):
    d["L%d"%i]=layer(hList[i],dirList[i],vList[i],strList[i],10+i)

r0=0.10
l0=25.
sourceList=[]

sourceList+=wfsOverview.values()
sourceList.append(sciOverview.getSciByID("b"))

atmosGeom=geom(d,sourceList,
	       ntel,npup,telDiam,r0,l0
	      )

#Now DM stuff.  2 DM objects needed here, even though there is only 1.  The first one generates the entire DM surface.  The second is for the DM metapupil - i.e. for non-ground-conjugate DMs, will select only the relevant line of sight.
from util.dm import dmOverview,dmInfo
dmHeight=0.
dmInfoList=[dmInfo('dm',['a'],dmHeight,wfs_nsubx+1,fov=fov,minarea=0.1,actuatorsFrom="recon",pokeSpacing=None,maxActDist=1.5,decayFactor=decayFactor,sendFullDM=1,reconLam=ngsLam),#sendFullDM must be set for wideField.
dmInfo('dmNF',['b'],dmHeight,wfs_nsubx+1,fov=fov,minarea=0.1,actuatorsFrom="Nothing",pokeSpacing=None,maxActDist=1.5,decayFactor=decayFactor,sendFullDM=0,reconLam=ngsLam)
]
dmOverview=dmOverview(dmInfoList,atmosGeom)


seed=1

this.wfscent_a=new()
this.wfscent_a.imageOnly=1

#reconstructor parameters.
this.tomoRecon=new()
this.tomoRecon.rcond=0.05
this.tomoRecon.recontype="pinv"
this.tomoRecon.pokeval=1.
this.tomoRecon.gainFactor=0.9
this.tomoRecon.computeControl=1
this.tomoRecon.reconmxFilename="rmx.fits"
this.tomoRecon.pmxFilename="pmx.fits"
