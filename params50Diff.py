#Taken from params50.py
import base.readConfig
this=base.readConfig.init(globals())
tstep=1/2000.#iteration time step.
AOExpTime=10.

wfs_sig=1e6 #wfs flux - but actually not used (taken from the solar images)

#number of phase pupils.
npup=400

#telescope diameter
telDiam=4.
ntel=npup
wfs_nsubx=50#number of subaps
wfs_n=npup/wfs_nsubx
ngsLam=500.#640.#ngs wavelength.
sciLam=500.#1650.#sci wavelength in nm
nlayer=20
decayFactor=0.99#integrator decay.
fov=5.#only used in param file - fov of the wfs.
nFieldX=6#number of fields to evaluate to make the shs image.
widefieldImageBoundary=8# The extra rows/cols added to the psf. This will ideally depend on pixel scale and seeing - i.e. should be equal to likely maximum spot motion
import util.FITS
data=util.FITS.Read("imsol.fits")[1]#imsol is 50 arcsec with 3600 pixels.
offsetx=this.getVal("offsetx",1000)
offsety=this.getVal("offsety",100)
npixels=int(3600/50.*fov*2)
data=data[offsety:offsety+npixels,offsetx:offsetx+npixels]*10
#Note - for this one above to work, need centroidPower=2.  And
#wide-field image boundary of 8.

from scipy.ndimage import interpolation
b=widefieldImageBoundary
fftsize=wfs_n*2
n=fftsize*(nFieldX+1.)/2
#For a different image per subap use this one:
#data=interpolation.zoom(data,wfs_nsubx*(n+2*b)/data.shape[0])
#For identical image per subap, use this one:
data=interpolation.zoom(data,(n+2*b)/data.shape[0])*100
widefieldImage=numpy.zeros((wfs_nsubx,wfs_nsubx,int(n+2*b),int(n+2*b)),numpy.float32)
for i in range(wfs_nsubx):
    for j in range(wfs_nsubx):
        widefieldImage[i,j]=data # use this for same image per subap.

studySubap=(3,3)
telSec=0.
import util.tel
spider=None
pupil=util.tel.Pupil(npup,ntel/2,ntel/2*telSec/telDiam,spider=spider)

layerList={"L0-2":["L%d"%x for x in range(nlayer)]}

from scipy.ndimage import interpolation
nimg=int(wfs_n*(nFieldX+1.)/2)
ncen=this.getVal("ncen",nimg//2)
corrPattern=numpy.zeros((wfs_nsubx,wfs_nsubx,nimg,nimg),"f")
#for i in range(wfs_nsubx):
#    for j in range(wfs_nsubx):
#        interpolation.zoom(widefieldImage[i,j],float(nimg)/widefieldImage.shape[2],output=corrPattern[i,j,nimg//2:nimg//2+nimg,nimg//2:nimg//2+nimg])


import util.guideStar
#create the wfs objects:  First one is for widefield image generation
wfsOverview=util.guideStar.wfsOverview({"a":util.guideStar.NGS("a",wfs_nsubx,0.,0.,npup/wfs_nsubx,sig=wfs_sig,sourcelam=ngsLam,fov=fov,pupil=pupil,spotpsf=numpy.ones((wfs_nsubx,wfs_nsubx,npup/wfs_nsubx*4,npup/wfs_nsubx*4),numpy.float32),floor=2500.),
#and this one is for the image -> slope module.
"acent":util.guideStar.NGS("acent",wfs_nsubx,0.,0.,npup/wfs_nsubx,sig=wfs_sig,sourcelam=ngsLam,fov=fov,pupil=pupil,nimg=nimg,ncen=ncen,correlationCentroiding=2,corrThresh=0.,corrPattern=corrPattern,cameraImage=1,reconList=["recon"],parabolicFit=1,centroidPower=1.0),
})

import util.sci
sciOverview=util.sci.sciOverview({"b":util.sci.sciInfo("b",0.,0.,pupil,sciLam,calcRMS=1),
"buncorr":util.sci.sciInfo("buncorr",0.,0.,pupil,sciLam,calcRMS=1),})

#Now atmosphere stuff
from util.atmos import geom,layer,source
import util.compare

zenith=0.#If zenith is changed, various things will automatically be scaled, including r0, layer heights, speeds and directions.

#cn2 profile from Luzma, defined for r0=10cm.
strList=[   0.907648 ,   0.0111941 ,   0.0116477  , 0.00907169  , 0.00695719,
0.00566037 ,  0.00550066 ,  0.00527910  , 0.00533289 ,  0.00591407,
0.00677259  , 0.00659962  , 0.00556733  , 0.00388403  , 0.00210666 ,
0.000733918 , 0.000122273, 7.70589e-006, 1.02995e-007 ,3.23286e-009])

hList=[0.,1130,2260,3390,4520,5650,6780,7910,9040,10170,11300.0,12430.0,13560.0,
14690.0,15820.0,16950.0,18080.0,19210.0,20340.0,21470.0]

vList=numpy.array([15.,12.61,8.73,5.,4.]+list((numpy.arange(nlayer-5)/7.+5)))
dirList=numpy.arange(nlayer)*36

seed=this.getVal("seed",1)

d={}
for i in range(nlayer):
    d["L%d"%i]=layer(hList[i],dirList[i],vList[i],strList[i],seed+i)

r0=this.getVal("r0",0.10)
if r0!=0.1:
    if r0>0:
        #scale ground layer strength so that turb of upper layers remains the same.
        strList[1:]*=(r0/0.1)**(5/3.)
    else:#scale all layers (this will happen by default).
        r0=-r0
    
l0=25.
sourceList=[]

sourceList+=wfsOverview.values()
sourceList.append(sciOverview.getSciByID("b"))

atmosGeom=geom(d,sourceList,ntel,npup,telDiam,r0,l0,zenith=zenith)

#Now DM stuff.  2 DM objects needed here, even though there is only 1.  The first one generates the entire DM surface.  The second is for the DM metapupil - i.e. for non-ground-conjugate DMs, will select only the relevant line of sight.
from util.dm import dmOverview,dmInfo
dmHeight=0.
dmInfoList=[dmInfo('dm',['a'],dmHeight,wfs_nsubx+1,fov=fov,minarea=0.1,actuatorsFrom="recon",pokeSpacing=10,maxActDist=1.5,decayFactor=decayFactor,sendFullDM=1,reconLam=ngsLam),#sendFullDM must be set for wideField.
dmInfo('dmNF',['b'],dmHeight,wfs_nsubx+1,fov=fov,minarea=0.1,actuatorsFrom="Nothing",pokeSpacing=10,maxActDist=1.5,decayFactor=decayFactor,sendFullDM=0,reconLam=ngsLam)
]
dmOverview=dmOverview(dmInfoList,atmosGeom)



this.wfscent_a=new()
this.wfscent_a.imageOnly=1

#reconstructor parameters.
this.tomoRecon=new()
this.tomoRecon.rcond=this.getVal("rcond",0.02)
this.tomoRecon.recontype="pinv"
this.tomoRecon.pokeval=1.
this.tomoRecon.gainFactor=this.getVal("gainFactor",0.6)
this.tomoRecon.computeControl=1
this.tomoRecon.reconmxFilename="rmx.fits"
this.tomoRecon.pmxFilename="pmx.fits"
