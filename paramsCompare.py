#Taken from params50.py
import base.readConfig
this=base.readConfig.init(globals())
tstep=1/2000.#iteration time step.
AOExpTime=10.

wfs_sig=1e6 #wfs flux - but actually not used (taken from the solar images)

#number of phase pupils.
npup=400

#telescope diameter
telDiam=4.
ntel=npup
wfs_nsubx=50#number of subaps
wfs_n=npup/wfs_nsubx
ngsLam=500.#640.#ngs wavelength.
sciLam=500.#1650.#sci wavelength in nm
nlayer=20
decayFactor=0.99#integrator decay.
fov=5.#only used in param file - fov of the wfs.
nFieldX=6#number of fields to evaluate to make the shs image.
widefieldImageBoundary=8# The extra rows/cols added to the psf. This will ideally depend on pixel scale and seeing - i.e. should be equal to likely maximum spot motion
import util.FITS
data=util.FITS.Read("imsol.fits")[1]#imsol is 50 arcsec with 3600 pixels.
offsetx=this.getVal("offsetx",1000)
offsety=this.getVal("offsety",100)
npixels=int(3600/50.*fov*2)
sigscale=this.getVal("sigscale",1.)
data=data[offsety:offsety+npixels,offsetx:offsetx+npixels]*10*sigscale
#Note - for this one above to work, need centroidPower=2.  And
#wide-field image boundary of 8.

from scipy.ndimage import interpolation
b=widefieldImageBoundary
fftsize=wfs_n*2
n=fftsize*(nFieldX+1.)/2
#For a different image per subap use this one:
#data=interpolation.zoom(data,wfs_nsubx*(n+2*b)/data.shape[0])
#For identical image per subap, use this one:
data=interpolation.zoom(data,(n+2*b)/data.shape[0])*100
widefieldImage=numpy.zeros((wfs_nsubx,wfs_nsubx,int(n+2*b),int(n+2*b)),numpy.float32)
for i in range(wfs_nsubx):
    for j in range(wfs_nsubx):
        widefieldImage[i,j]=data # use this for same image per subap.

studySubap=(3,3)
telSec=0.
import util.tel
spider=None
pupil=util.tel.Pupil(npup,ntel/2,ntel/2*telSec/telDiam,spider=spider)

layerList={"allLayers":["L%d"%x for x in range(nlayer)]}

from scipy.ndimage import interpolation
nimg=int(wfs_n*(nFieldX+1.)/2)
ncen=this.getVal("ncen",nimg//2)
corrPattern=numpy.zeros((wfs_nsubx,wfs_nsubx,nimg,nimg),"f")
#for i in range(wfs_nsubx):
#    for j in range(wfs_nsubx):
#        interpolation.zoom(widefieldImage[i,j],float(nimg)/widefieldImage.shape[2],output=corrPattern[i,j,nimg//2:nimg//2+nimg,nimg//2:nimg//2+nimg])
centroidPower=this.getVal("centroidPower",1.)
floor=this.getVal("floor",2500.)
readfloor=this.getVal("readfloor",0.)
parabolicFit=this.getVal("parabolicFit",1)
import util.guideStar
#create the wfs objects:  First one is for widefield image generation
wfsOverview=util.guideStar.wfsOverview({"a":util.guideStar.NGS("a",wfs_nsubx,0.,0.,npup/wfs_nsubx,sig=wfs_sig,sourcelam=ngsLam,fov=fov,pupil=pupil,spotpsf=numpy.ones((wfs_nsubx,wfs_nsubx,npup/wfs_nsubx*4,npup/wfs_nsubx*4),numpy.float32),floor=floor),
#and this one is for the image -> slope module.
"acent":util.guideStar.NGS("acent",wfs_nsubx,0.,0.,npup/wfs_nsubx,sig=wfs_sig,sourcelam=ngsLam,fov=fov,pupil=pupil,nimg=nimg,ncen=ncen,correlationCentroiding=2,corrThresh=0.,corrPattern=corrPattern,cameraImage=1,reconList=["recon"],parabolicFit=parabolicFit,centroidPower=centroidPower,threshType=0,floor=readfloor),
"1":util.guideStar.NGS("1",wfs_nsubx,0.,0.,phasesize=npup/wfs_nsubx,minarea=0.5,sig=1e6,sourcelam=ngsLam,reconList=["recon"],pupil=pupil)                                        
})


#Now atmosphere stuff
from util.atmos import geom,layer,source
import util.compare

#cn2 profile from Luzma
strList=[   0.907648 ,   0.0111941 ,   0.0116477  , 0.00907169  , 0.00695719,
0.00566037 ,  0.00550066 ,  0.00527910  , 0.00533289 ,  0.00591407,
0.00677259  , 0.00659962  , 0.00556733  , 0.00388403  , 0.00210666 ,
0.000733918 , 0.000122273, 7.70589e-006, 1.02995e-007 ,3.23286e-009]

hList=[0.,1130,2260,3390,4520,5650,6780,7910,9040,10170,11300.0,12430.0,13560.0,
14690.0,15820.0,16950.0,18080.0,19210.0,20340.0,21470.0]

vList=numpy.array([15.,12.61,8.73,5.,4.]+list((numpy.arange(nlayer-5)/7.+5)))
dirList=numpy.arange(nlayer)*36

d={}
for i in range(nlayer):
    d["L%d"%i]=layer(hList[i],dirList[i],vList[i],strList[i],10+i)

r0=0.10
l0=25.
sourceList=[]

sourceList+=wfsOverview.values()

atmosGeom=geom(d,sourceList,
	       ntel,npup,telDiam,r0,l0
	      )



seed=1

this.wfscent_a=new()
this.wfscent_a.imageOnly=1

