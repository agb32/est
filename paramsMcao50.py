#Taken from params2.xml
import numpy
import base.readConfig
this=base.readConfig.init(globals())
tstep=1/2000.#iteration time step.
AOExpTime=40.

wfs_sig=1e6 #wfs flux - but actually not used (taken from the solar images)

#telescope diameter
telDiam=4.
telSec=0.

wfs_nsubx=50#number of subaps
phasesize=8
npup=wfs_nsubx*phasesize
ntel=npup
nAct=wfs_nsubx+1
#wfs_n=npup/wfs_nsubx
ngsLam=500.#ngs wavelength.
sciLam=this.getVal("sciLam",500.)#sci wavelength in nm
wfsRadius=float(this.getVal("wfsRadius",30.))#arcseconds
nsci=1
nngs=6
ndm=this.getVal("ndm",5)
imgOffsetX=this.getVal("imgOffsetX",3600)
imgOffsetY=this.getVal("imgOffsetY",3600)
resFile=this.getVal("resFile","resMcao.csv")

fov=5.#only used in param file - half the fov of the wfs.
nFieldX=6#number of fields to evaluate to make the shs image.
widefieldImageBoundary=8# The extra rows/cols added to the psf. This will ideally depend on pixel scale and seeing - i.e. should be equal to likely maximum spot motion

studySubap=(3,3)
import util.tel
pupil=util.tel.Pupil(npup,ntel/2,ntel/2*telSec/telDiam)


nimg=int(phasesize*(nFieldX+1.)/2)
corrPattern=numpy.zeros((wfs_nsubx,wfs_nsubx,nimg,nimg),"f")

import util.guideStar
#create the wfs objects:  First one is for widefield image generation
wfsDict={}
sourceList=[]
for i in range(nngs):
    id="%d"%i
    wfsDict[id]=util.guideStar.NGS(id,wfs_nsubx,wfsRadius,i*(360./nngs),npup/wfs_nsubx,sig=wfs_sig,sourcelam=ngsLam,fov=fov,pupil=pupil,spotpsf=numpy.ones((wfs_nsubx,wfs_nsubx,npup/wfs_nsubx*4,npup/wfs_nsubx*4),numpy.float32),floor=2500.)
#and this one is for the image -> slope module.
    sourceList.append(wfsDict[id])
    id="%dcent"%i
    wfsDict[id]=util.guideStar.NGS(id,wfs_nsubx,wfsRadius,i*(360./nngs),npup/wfs_nsubx,sig=wfs_sig,sourcelam=ngsLam,fov=fov,pupil=pupil,nimg=nimg,ncen=nimg,correlationCentroiding=2,corrThresh=0.0,corrPattern=corrPattern,cameraImage=1,reconList=["recon"],parabolicFit=1,centroidPower=1.0)
    sourceList.append(wfsDict[id])

wfsOverview=util.guideStar.wfsOverview(wfsDict)    

import util.sci
sciOverview=util.sci.sciOverview({"b":util.sci.sciInfo("b",0.,0.,pupil,sciLam,calcRMS=1,summaryFilename=resFile),
"buncorr":util.sci.sciInfo("buncorr",0.,0.,pupil,sciLam,calcRMS=1,summaryFilename=resFile),})
sourceList.append(sciOverview.getSciByID("b"))
sourceList.append(sciOverview.getSciByID("buncorr"))
#Now atmosphere stuff
from util.atmos import geom,layer,source
import util.compare
#strList=[0.5,0.3,0.2]
#hList=[0.,4000.,10000.]
#vList=[4.55,12.61,8.73]
#dirList=numpy.arange(10)*36
#cn2 profile from Luzma
strList=[   0.907648 ,   0.0111941 ,   0.0116477  , 0.00907169  , 0.00695719,
0.00566037 ,  0.00550066 ,  0.00527910  , 0.00533289 ,  0.00591407,
0.00677259  , 0.00659962  , 0.00556733  , 0.00388403  , 0.00210666 ,
0.000733918 , 0.000122273, 7.70589e-006, 1.02995e-007 ,3.23286e-009]
nlayer=len(strList)
hList=[0.,1130,2260,3390,4520,5650,6780,7910,9040,10170,11300.0,12430.0,13560.0,
14690.0,15820.0,16950.0,18080.0,19210.0,20340.0,21470.0]
vList=numpy.array([15.,12.61,8.73,5.,4.]+list((numpy.arange(nlayer-5)/7.+5)))
dirList=numpy.arange(nlayer)*36

layerList={"allLayers":["L%d"%x for x in range(nlayer)]}

atmosDict={}
for i in range(nlayer):
    atmosDict["L%d"%i]=layer(hList[i],dirList[i],vList[i],strList[i],10+i)

l0=this.getVal("l0",25.) #outer scale
r0=this.getVal("r0",0.1) #fried's parameter
zenith=this.getVal("zenith",0.)
atmosGeom=geom(atmosDict,sourceList,ntel,npup,telDiam,r0,l0,zenith=zenith)



#Now DM stuff.  2 DM objects needed here, even though there is only 1.  The first one generates the entire DM surface.  The second is for the DM metapupil - i.e. for non-ground-conjugate DMs, will select only the relevant line of sight.
from util.dm import dmOverview,dmInfo
if ndm==5:
    dmHeight=[0.,5000.,9000.,12000.,15000.]
elif ndm==4:
    dmHeight=[0.,5000.,9000.,12000.]
elif ndm==3:
    dmHeight=[0.,5000.,12000.]
elif ndm==2:
    dmHeight=[0.,5000.]
else:
    dmHeight=[0]
strListdm=this.getVal("strListdm",numpy.array([0.9,0.0001,0.00008,0.00008,0.00008]))
#30cm spacing on upper DMs
nactList=[nAct]+[None]*(ndm-1)
actSpacing=[None]+[0.3]*(ndm-1)

                    
dmInfoList=[]
for i in range(ndm):
    dmInfoList.append(dmInfo('dm%d_'%i,['%d'%j for j in range(nngs)],dmHeight[i],nactList[i],fov=fov+wfsRadius,minarea=0.1,actuatorsFrom="recon",pokeSpacing=(None if wfs_nsubx<20 else 10),maxActDist=1.5,decayFactor=None,sendFullDM=1,reconLam=ngsLam,actSpacing=actSpacing[i]))#sendFullDM must be set for wideField.
    dmInfoList.append(dmInfo('dmNF%d'%i,['b'],dmHeight[i],nactList[i],fov=fov+wfsRadius,minarea=0.1,actuatorsFrom="Nothing",pokeSpacing=(None if wfs_nsubx<20 else 10),maxActDist=1.5,decayFactor=None,sendFullDM=0,reconLam=ngsLam,actSpacing=actSpacing[i]))

dmOverview=dmOverview(dmInfoList,atmosGeom)


seed=1
for i in range(nngs):
    setattr(this,"wfscent_%d"%i,new())
    getattr(this,"wfscent_%d"%i).imageOnly=1


import util.FITS
from scipy.ndimage import interpolation
data2=util.FITS.Read("imsol.fits")[1]#imsol is 50 arcsec with 3600 pixels.  Need to tile it to make it larger.
pxlPerArcsec=3600/50.
data=numpy.zeros((7200,7200),numpy.float32)
data[:3600,:3600]=data2
data[:3600,3600:]=data2
data[3600:,:3600]=data2
data[3600:,3600:]=data2
for i in range(nngs):
    setattr(this,"wideField_%d"%i,new())
    wf=getattr(this,"wideField_%d"%i)
    wfs=i
    print "Making widefield image for module wideField_%d"%i
    #shape should be
    #(nsubx,nsubx,fftsize*(nFieldX+1)/2,fftsize*(nFieldX+1)/2)
    offsetx=int(imgOffsetX+wfsRadius*numpy.cos(wfs*360./nngs*numpy.pi/180.)*pxlPerArcsec)
    offsety=int(imgOffsetY+wfsRadius*numpy.sin(wfs*360./nngs*numpy.pi/180.)*pxlPerArcsec)
    npixels=int(3600/50.*fov*2)
    print "Image offset for wfs %d is %d, %d (to %d %d)"%(wfs,offsetx,offsety,offsetx+npixels,offsety+npixels)
    if offsetx<0 or offsety<0 or (offsetx+npixels)>data.shape[1] or (offsety+npixels)>data.shape[0]:
        print "%d %d %d %d %d %d"%(offsetx,offsety,offsetx+npixels,offsety+npixels,data.shape[1],data.shape[0])
        raise Exception("Offsets not good for image selection")
    data2=data[offsety:offsety+npixels,offsetx:offsetx+npixels]*10
    #Note - for this one above to work, need centroidPower=2.  And
    #wide-field image boundary of 8.

    b=widefieldImageBoundary
    fftsize=phasesize*2
    n=fftsize*(nFieldX+1.)/2
    #For a different image per subap use this one:
    #data=interpolation.zoom(data,wfs_nsubx*(n+2*b)/data.shape[0])
    #For identical image per subap, use this one:
    data2=interpolation.zoom(data2,(n+2*b)/data2.shape[0])*100
    widefieldImage=numpy.zeros((wfs_nsubx,wfs_nsubx,int(n+2*b),int(n+2*b)),numpy.float32)
    for i in range(wfs_nsubx):
        for j in range(wfs_nsubx):
            widefieldImage[i,j]=data2 # use this for same image per subap.
    wf.widefieldImage=widefieldImage


    
#reconstructor parameters.
this.tomoRecon=new()
this.tomoRecon.rcond=0.05
this.tomoRecon.recontype="pinv"
this.tomoRecon.pokeval=1.
this.tomoRecon.gainFactor=this.getVal("gainFactor",0.5)
this.tomoRecon.computeControl=0
this.tomoRecon.abortAfterPoke=1
this.tomoRecon.reconmxFilename=this.getVal("reconmxFilename","rmx.fits")
this.tomoRecon.pmxFilename="pmx.fits"

#used in the reconstruct2.py:
scale=this.getVal("scale",10.)
subtractTT=0
reconIdStr="recon"
layerPower=this.getVal("layerPower",1.)
noiseCovScale=this.getVal("noiseCovScale",1.)

globalPolc="polc.fits"
