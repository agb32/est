#Taken from params2.xml
import base.readConfig
base.readConfig.init(globals())
tstep=1/250.#iteration time step.
AOExpTime=40.

wfs_sig=1e6 #wfs flux - but actually not used (taken from the solar images)

#number of phase pupils.
npup=80

#telescope diameter
telDiam=4.
ntel=npup
wfs_nsubx=10#number of subaps
wfs_n=npup/wfs_nsubx
ngsLam=640.#ngs wavelength.
sciLam=1650.#sci wavelength in nm
nlayer=3
if hasattr(this.globals,"nwfs"):
    nwfs=this.globals.nwfs
else:
    nwfs=3
ndm=3
if hasattr(this.globals,"wfsRadius"):
    wfsRadius=this.globals.wfsRadius
else:
    wfsRadius=5.
if hasattr(this.globals,"imgOffsetX"):
    imgOffsetX=this.globals.imgOffsetX
else:
    imgOffsetX=600
if hasattr(this.globals,"imgOffsetY"):
    imgOffsetY=this.globals.imgOffsetY
else:
    imgOffsetY=1520
if hasattr(this.globals,"resFile"):
    resFile=this.globals.resFile
else:
    resFile="resMcao.csv"
decayFactor=0.99#integrator decay.
fov=5.#only used in param file - fov of the wfs.
nFieldX=6#number of fields to evaluate to make the shs image.
widefieldImageBoundary=8# The extra rows/cols added to the psf. This will ideally depend on pixel scale and seeing - i.e. should be equal to likely maximum spot motion

studySubap=(3,3)
telSec=0.
import util.tel
spider=None
pupil=util.tel.Pupil(npup,ntel/2,ntel/2*telSec/telDiam,spider=spider)

layerList={"allLayers":["L%d"%x for x in range(nlayer)]}

nimg=int(wfs_n*(nFieldX+1.)/2)
corrPattern=numpy.zeros((wfs_nsubx,wfs_nsubx,nimg,nimg),"f")
#for i in range(wfs_nsubx):
#    for j in range(wfs_nsubx):
#        interpolation.zoom(widefieldImage[i,j],float(nimg)/widefieldImage.shape[2],output=corrPattern[i,j,nimg//2:nimg//2+nimg,nimg//2:nimg//2+nimg])


import util.guideStar
#create the wfs objects:  First one is for widefield image generation
wfsDict={}
for i in range(nwfs):
    wfsDict["%d"%i]=util.guideStar.NGS("%d"%i,wfs_nsubx,wfsRadius,i*(360./nwfs),npup/wfs_nsubx,sig=wfs_sig,sourcelam=ngsLam,fov=fov,pupil=pupil,spotpsf=numpy.ones((wfs_nsubx,wfs_nsubx,npup/wfs_nsubx*4,npup/wfs_nsubx*4),numpy.float32),floor=2500.)
#and this one is for the image -> slope module.
    wfsDict["%dcent"%i]=util.guideStar.NGS("%dcent"%i,wfs_nsubx,wfsRadius,i*(360./nwfs),npup/wfs_nsubx,sig=wfs_sig,sourcelam=ngsLam,fov=fov,pupil=pupil,nimg=nimg,ncen=nimg,correlationCentroiding=2,corrThresh=0.0,corrPattern=corrPattern,cameraImage=1,reconList=["recon"],parabolicFit=1,centroidPower=1.0)

wfsOverview=util.guideStar.wfsOverview(wfsDict)    

import util.sci
sciOverview=util.sci.sciOverview({"b":util.sci.sciInfo("b",0.,0.,pupil,sciLam,calcRMS=1,summaryFilename=resFile),
"buncorr":util.sci.sciInfo("buncorr",0.,0.,pupil,sciLam,calcRMS=1,summaryFilename=resFile),})

#Now atmosphere stuff
from util.atmos import geom,layer,source
import util.compare
strList=[0.5,0.3,0.2]
hList=[0.,4000.,10000.]
vList=[4.55,12.61,8.73]
dirList=numpy.arange(10)*36
d={}
for i in range(nlayer):
    d["L%d"%i]=layer(hList[i],dirList[i],vList[i],strList[i],10+i)

r0=0.10
l0=25.
sourceList=[]

sourceList+=wfsOverview.values()
sourceList.append(sciOverview.getSciByID("b"))

atmosGeom=geom(d,sourceList,
	       ntel,npup,telDiam,r0,l0
	      )

#Now DM stuff.  2 DM objects needed here, even though there is only 1.  The first one generates the entire DM surface.  The second is for the DM metapupil - i.e. for non-ground-conjugate DMs, will select only the relevant line of sight.
from util.dm import dmOverview,dmInfo
dmHeight=[0.,4000.,10000.]
dmInfoList=[]
for i in range(ndm):
    dmInfoList.append(dmInfo('dm%d_'%i,['%d'%j for j in range(nwfs)],dmHeight[i],wfs_nsubx+1,fov=fov+wfsRadius,minarea=0.1,actuatorsFrom="recon",pokeSpacing=None,maxActDist=1.5,decayFactor=decayFactor,sendFullDM=1,reconLam=ngsLam))#sendFullDM must be set for wideField.
    dmInfoList.append(dmInfo('dmNF%d'%i,['b'],dmHeight[i],wfs_nsubx+1,fov=fov+wfsRadius,minarea=0.1,actuatorsFrom="Nothing",pokeSpacing=None,maxActDist=1.5,decayFactor=decayFactor,sendFullDM=0,reconLam=ngsLam))

dmOverview=dmOverview(dmInfoList,atmosGeom)


seed=1
for i in range(nwfs):
    setattr(this,"wfscent_%d"%i,new())
    getattr(this,"wfscent_%d"%i).imageOnly=1


import util.FITS
from scipy.ndimage import interpolation
for i in range(nwfs):
    setattr(this,"wideField_%d"%i,new())
    wf=getattr(this,"wideField_%d"%i)
    wfs=i
    print "Making widefield image for module wideField_%d"%i
    #shape should be
    #(nsubx,nsubx,fftsize*(nFieldX+1)/2,fftsize*(nFieldX+1)/2)
    data=util.FITS.Read("imsol.fits")[1]#imsol is 50 arcsec with 3600 pixels.
    pxlPerArcsec=3600/50.
    offsetx=int(imgOffsetX+wfsRadius*numpy.cos(wfs*360./nwfs*numpy.pi/180.)*pxlPerArcsec)
    offsety=int(imgOffsetY+wfsRadius*numpy.sin(wfs*360./nwfs*numpy.pi/180.)*pxlPerArcsec)
    print "Image offset for wfs %d is %d, %d"%(wfs,offsetx,offsety)
    npixels=int(3600/50.*fov*2)
    if offsetx<0 or offsety<0 or (offsetx+npixels)>data.shape[1] or (offsety+npixels)>data.shape[0]:
        raise Exception("Offsets not good for image selection")
    data=data[offsety:offsety+npixels,offsetx:offsetx+npixels]*10
    #Note - for this one above to work, need centroidPower=2.  And
    #wide-field image boundary of 8.

    b=widefieldImageBoundary
    fftsize=wfs_n*2
    n=fftsize*(nFieldX+1.)/2
    #For a different image per subap use this one:
    #data=interpolation.zoom(data,wfs_nsubx*(n+2*b)/data.shape[0])
    #For identical image per subap, use this one:
    data=interpolation.zoom(data,(n+2*b)/data.shape[0])*100
    widefieldImage=numpy.zeros((wfs_nsubx,wfs_nsubx,n+2*b,n+2*b),numpy.float32)
    for i in range(wfs_nsubx):
        for j in range(wfs_nsubx):
            widefieldImage[i,j]=data # use this for same image per subap.
    wf.widefieldImage=widefieldImage


    
#reconstructor parameters.
this.tomoRecon=new()
this.tomoRecon.rcond=0.05
this.tomoRecon.recontype="pinv"
this.tomoRecon.pokeval=1.
this.tomoRecon.gainFactor=0.5
this.tomoRecon.computeControl=1
this.tomoRecon.reconmxFilename="rmx.fits"
this.tomoRecon.pmxFilename="pmx.fits"
