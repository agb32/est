"""
Solve:

Rmx = (S^T Cn^-1 S + Cw^-1 )^-1 S^T Cn^-1

e.g. Thiebaut 2010,  Fast minimum variance wavefront reconstruction for extremely large telescopes.

Taken from mosaic/base/reconstruct.py

"""
import os
import numpy
import util.computeRecon
import util.FITS
import base.readConfig
import util.gradientOperator
import util.dm
import util.Ctrl
import sys
import time
from scipy.special import kv,gamma
tstart=time.time()
sys.argv.append("--nostdin")
ctrl=util.Ctrl.Ctrl(globals=globals())
c=ctrl.config
c.setSearchOrder(["tomoRecon","globals"])
pmxfname=c.getVal("pmxFilename")
scale=c.getVal("scale",default=0)
scale2=c.getVal("scale2",default=0)
noiseCovScale=c.getVal("noiseCovScale",default=1.)
scalePxlScale=c.getVal("scalePxlScale",default=1)
theoreticalNoiseCov=c.getVal("theoreticalNoiseCov",default=0)
subtractTT=c.getVal("subtractTT",default=1)
header=[]#info for the fits header.

idstr=c.getVal("reconIdStr")
outputName=c.getVal("reconmxFilename")
saveIntermediate=c.getVal("saveIntermediate",default=0)
dmObj=c.getVal("dmOverview")
atmosGeom=c.getVal("atmosGeom")
L0=atmosGeom.l0
telDiam=atmosGeom.telDiam
pup=c.getVal("pupil")
dmList=dmObj.makeDMList(idstr)
print "Computing nact list"
nactsList=numpy.array(util.dm.calcNactList(c,0,idstr))
nactsCumList=numpy.insert(nactsList.cumsum(),0,0)
autoscale=0
if scale==0:
    scale=1
    autoscale=1
strListDm=numpy.array(c.getVal("strListdm"))
layerPower=c.getVal("layerPower",default=1.)
print "LayerPower: %g"%layerPower
adjustLayerForConeEffect=c.getVal("adjustLayerForConeEffect",default=0)
strListDm/=strListDm.sum()
if adjustLayerForConeEffect:
    alt=c.getVal("lgsAlt")
    hlist=numpy.array(c.getVal("hListdm"))
    if alt>0:
        if numpy.any(hlist>alt):
            raise Exception("Unhandled - layer above LGS")
        strListDm*=((alt-hlist)/alt)**(5./3)
cn2=1./strListDm**layerPower*scale
spacingScale=(numpy.array([x.actSpacing for x in dmList])/dmList[0].actSpacing)**(-5./3)
print "Scaling DM Strengths by %s"%str(spacingScale)
cn2*=spacingScale
print "Final layer weightings are %s"%str(cn2)


laplist=[]
for i in range(len(dmList)):
    dm=dmList[i]
    dmpup=dm.computeDMPupil(atmosGeom,pup.r2,retPupil=0)[0].astype("f")
    g=util.gradientOperator.gradientOperatorType1(pupilMask=dmpup,sparse=1)
    #util.gradientOperator.genericLaplacianCalcOp_NumpyArray(g)#scipyCSR
    util.gradientOperator.genericLaplacianCalcOp_scipyCSR(g)
    #print g.op.dtype
    laplacian2=numpy.dot(g.op,g.op)
    #print laplacian2.dtype,cn2.dtype
    laplacian2*=cn2[i]
    #print "laplist shape %s"%str(laplacian2.shape)
    laplist.append(laplacian2)
    if saveIntermediate:
        util.FITS.Write(laplacian2.todense(),"laplist.fits",writeMode=('w' if i==0 else 'a'))
ll=numpy.array([x.shape[0] for x in laplist])
lapCumList=numpy.insert(ll.cumsum(),0,0)
print "lapCumList %s"%str(lapCumList)
print "nactsCumList %s"%str(nactsCumList)
for i in range(len(nactsCumList)):
    if lapCumList[i]!=nactsCumList[i]:
        print "Actuator counts don't agree (maxActDist may need changing)"

#First, separate out the lgs and ngs.  In the rmx, ngs come first.
ngsList=atmosGeom.makeNGSList(idstr)
lgsList=atmosGeom.makeLGSList(idstr)
gsList=ngsList+lgsList
ncents=0
ncentNgsList=[]
ncentLgsList=[]
indiceNgsList=[]
indiceLgsList=[]
lgssigList=[]
ngssigList=[]
ngsPxlscaleList=[]
lgsPxlscaleList=[]
for gs in ngsList:
    subflag=gs.getSubapFlag()
    indiceNgsList.append(numpy.flatnonzero(subflag))
    ncentNgsList.append(subflag.sum())
    ngssigList.append(gs.sig)
    if scalePxlScale:
        ngsPxlscaleList.append(gs.getPxlScale(telDiam))
    else:
        ngsPxlscaleList.append(1.)
for gs in lgsList:
    subflag=gs.getSubapFlag()
    indiceLgsList.append(numpy.flatnonzero(subflag))
    ncentLgsList.append(subflag.sum())
    lgssigList.append(gs.sig)
    if scalePxlScale:
        lgsPxlscaleList.append(gs.getPxlScale(telDiam))
    else:
        lgsPxlscaleList.append(1.)
ncentList=ncentNgsList+ncentLgsList
ncentsNgs=sum(ncentNgsList)*2
ncentsLgs=sum(ncentLgsList)*2
ncents=ncentsNgs+ncentsLgs
nsubaps=ncents/2
nngssubaps=ncentsNgs/2
print "ncents:",ncents
print ngssigList,lgssigList
print ngsPxlscaleList
print lgsPxlscaleList
pxlScale=ngsPxlscaleList+lgsPxlscaleList
#poke matrix can be scaled into arcseconds.
pmx=util.FITS.Read(pmxfname)[1]
if pmx.shape!=(nactsCumList[-1],ncents):
    raise Exception("pmx wrong shape %s, should be %d %d"%(str(pmx.shape),nactsCumList[-1],ncents))
s=0
for i in range(len(gsList)):
    gs=gsList[i]
    e=s+ncentList[i]
    pmx[:,s:e]*=pxlScale[i]
    pmx[:,s+nsubaps:e+nsubaps]*=pxlScale[i]
    print pxlScale[i]
    s=e

#Already scaled to per radian during the poking... so the pmx is now in arcsec per radian.

    
#compute the noise covariance...
#noise covariance will be in radians squared.
invNoiseCov=numpy.zeros((ncents,),numpy.float32)
invNoiseCovNgs=numpy.zeros((ncentsNgs,),numpy.float32)
s=0
sngs=0
if theoreticalNoiseCov==1:
    start=0
    for i in range(len(gsList)):
        gs=gsList[i]
        tpxlscale=gs.getPxlScale(telDiam)/3600./180.*numpy.pi#radians per pixel
        s=0.0251*gs.sourcelam/(atmosGeom.r0*100)/3600./180.*numpy.pi#seeing angle in radians
        d=telDiam/gs.nsubx#size of subap in same unit as sigma (sqrt(var))
        F=gs.sig#number of electrons per subap per frame
        e=gs.readoutNoise#rms readout noise
        z=tpxlscale#pixel size (in radians)
        if F!=0:
            if gs.alt==-1:#ngs
                var=0.32*d**2*s**2/F + 0.82*d**2*s**4/z**2*(e/F)**2
            else:
                elong=tpxlscale*gs.nimg*0.75#elongation in radians - assume 75% of a subap...
                w=0.8/3600./180.*numpy.pi#unelongated width of laser beam in radians. - 0.8arcsec
                var=0.32*d**2*(s**2+elong**2+w**2)/F + 0.82*d**2*(s**2+w**2+elong**2)**1.5*(s**2+w**2)**.5/z**2*(e/F)**2
        else:
            var=0.
        header.append(("WFSN%s    "%(gs.idstr))[:8]+"= %g"%(numpy.sqrt(var)*1e9))
        end=start+ncentList[i]
        print "Variance: %g rms wfe in nm: %g"%(var,numpy.sqrt(var)*1e9)
        if var!=0:
            invNoiseCov[start:end]=1./var
            invNoiseCov[start+nsubaps:end+nsubaps]=1./var
            if gs.alt==-1:
                engs=sngs+ncentList[i]
                invNoiseCovNgs[sngs:engs]=1./var
                invNoiseCovNgs[sngs+nngssubaps:engs+nngssubaps]=1./var
                sngs=engs
        start=end
elif theoreticalNoiseCov==2:#the old method
    print "Using old method for inv noise cov..."
    lgssig=c.getVal("lgssig")#changed from wfs_sig
    wfsSigList=c.getVal("wfsSigList")
    start=0
    if sum(wfsSigList)==0:
        print "No NGS specified.  Keeping LGS TT"
        subtractTT=0
    noiseScale=1.
    noisePower=0.4
    for i in range(len(ngsList)):
        gs=ngsList[i]
        end=start+ncentNgsList[i]
        if wfsSigList[i]==0:
            inc=0
        else:
            inc=(wfsSigList[i]/1e6*noiseScale)**noisePower
        invNoiseCov[start:end]=inc
        invNoiseCovNgs[start:end]=inc
        invNoiseCov[start+ncents/2:end+ncents/2]=inc
        invNoiseCovNgs[start+ncentsNgs/2:end+ncentsNgs/2]=inc
        start=end
    for i in range(len(lgsList)):
        gs=lgsList[i]
        end=start+ncentLgsList[i]
        invNoiseCov[start:end]=(lgssig/1e6*noiseScale)**noisePower
        invNoiseCov[start+ncents/2:end+ncents/2]=(lgssig/1e6*noiseScale)**noisePower
        start=end
elif theoreticalNoiseCov==3:#just add a constant...
    print "Using a constant for noise covariance"
    invNoiseCov[:]=1.#gets scaled later
else:#use the monte-carlo noise covariance.
    covTag=c.getVal("covarianceFileTag",default="")
    for i in range(len(gsList)):
        gs=gsList[i]
        covfname="wfsCovariance_%s%s_%g_%g.fits"%(covTag,gs.idstr,gs.sig,gs.readoutNoise)
        print "Reading covariance %s"%covfname
        cov=util.FITS.Read(covfname)[1]
        e=s+ncentList[i]
        indx=numpy.flatnonzero(gs.getSubapFlag())
        invNoiseCov[s:e]=1./(cov[:,:,0].ravel()[indx]*pxlScale[i]**2)
        invNoiseCov[s+nsubaps:e+nsubaps]=1./(cov[:,:,1].ravel()[indx]*pxlScale[i]**2)
        if gs.alt==-1:#ngs.
            engs=sngs+ncentList[i]
            invNoiseCovNgs[sngs:engs]=invNoiseCov[s:e]
            invNoiseCovNgs[sngs+nngssubaps:engs+nngssubaps]=invNoiseCov[s+nsubaps:e+nsubaps]
            sngs=engs
        header.append(("WFSN%s    "%(gs.idstr))[:8]+"= %g"%(1/numpy.sqrt(invNoiseCov[s:e].mean())))#rms spot motion in arcsec
        s=e
print "Scaling noisecov by %g"%noiseCovScale
invNoiseCov*=noiseCovScale
if saveIntermediate:
    util.FITS.Write(invNoiseCov,"invNoiseCov.fits")
#invNoiseCov[:]=1
#invNoiseCovNgs[:]=1
#now separate the ngs and lgs for split tomo.
#First, use both lgs and ngs for high order.

#and now just the ngs part:
#ngspmx=numpy.empty((nactsCumList[-1],ncentsNgs),numpy.float32)
#ngspmx[:,:nngssubaps]=pmx[:,:nngssubaps]
#ngspmx[:,nngssubaps:]=pmx[:,nsubaps:nsubaps+nngssubaps]

#Now, if sig for any part of 0, set to zero.
s=0
resave=0
for i in range(len(ngsList)):
    gs=ngsList[i]
    e=s+ncentNgsList[i]
    if ngssigList[i]==0:
        resave=1
        print "Ignoring NGS %s"%gs.idstr
        #ngspmx[:,s:e]=0
        #ngspmx[:,s+nngssubaps:e+nngssubaps]=0
        pmx[:,s:e]=0
        pmx[:,s+nsubaps:e+nsubaps]=0
    s=e
#util.FITS.Write(ngspmx,"ngspmx.fits",doByteSwap=0)
#if resave:
#    print "Resaving %s with 0 for non-present NGSs"%pmxfname
#    raise Exception("Actually - can't do this because we've rescaled it...")
    #util.FITS.Write(pmx,"pmx.fits",doByteSwap=0)


r=util.computeRecon.makeRecon(pmxfname,scale)

res=r.dotdot(pmx,diagScaling=invNoiseCov,save=0)
print "Max of dotdot: %g %g"%(res.min(),res.max())
if saveIntermediate:
    util.FITS.Write(res,"dotdot.fits")

if autoscale:
    #try to compute the scaling based on max values...
    mx=res.max()
    mxlist=numpy.array([l.max() for l in laplist]).mean()
    ratio=mxlist/mx#want ratio to be about 0.1
    scale=0.1/ratio
    for l in laplist:
        l*=scale
    print "Computing scale to be %g"%(scale)
    header.append("SCALE1  = %g"%scale)
    
#Now add the laplacian.
for i in range(len(nactsCumList)-1):
    res[nactsCumList[i]:nactsCumList[i+1],nactsCumList[i]:nactsCumList[i+1]]+=laplist[i]
    print "lap max/min: %g %g"%(laplist[i].min(),laplist[i].max())
if saveIntermediate:
    util.FITS.Write(res,"dotdotlaplist.fits")


#Now the inversion:
t1=time.time()
inv=r.makeLUInv(res,save=0)
print "Inversion took %g s"%(time.time()-t1)
if saveIntermediate:
    util.FITS.Write(inv,"inv.fits")

#final part...
#rmx=r.denseDotDense(transA=1,diagScaling=invNoiseCov,save=0)

#Scale pmx by invNoiseCov...
for i in xrange(pmx.shape[1]):
    pmx[:,i]*=invNoiseCov[i]
rmx=r.dot(inv,pmx).T

#print "TODO::: denseDotDense without loading from disk"
#os.unlink(r.invname)
if subtractTT:
    #Now subtract tip-tilt.
    pos=0
    for i in range(len(ncentList)):
        nx=ncentList[i]
        colsum=rmx[pos:pos+nx].sum(0)/nx
        rmx[pos:pos+nx]-=colsum

        colsum=rmx[pos+nsubaps:pos+nsubaps+nx].sum(0)/nx
        rmx[pos+nsubaps:pos+nsubaps+nx]-=colsum
        pos+=nx
    print "TT removed"

    if saveIntermediate:
        print "Saving HO rmx"
        util.FITS.Write(rmx,outputName[:-5]+"HO.fits")

    #Now do just the NGS - low order.
    r=util.computeRecon.makeRecon("nowt.fits",scale)
    #remove the LGS components of the pmx.
    pmx[:,nngssubaps:2*nngssubaps]=pmx[:,nsubaps:nsubaps+nngssubaps]
    ngspmx=pmx[:,:2*nngssubaps]

    res=r.dotdot(ngspmx,diagScaling=invNoiseCovNgs,save=0)
    print "Max/min of dotdot: %g %g"%(res.min(),res.max())


    if autoscale:
        #try to compute the scaling based on max values...
        mx=res.max()
        mxlist=numpy.array([l.max() for l in laplist]).mean()/scale
        ratio=mxlist/mx#want ratio to be about 0.1
        scale2=0.1/ratio
        #for l in laplist:
        #    l*=scale2
        print "Computing scale to be %g"%(scale2)
        header.append("SCALE2  = %g"%scale2)
    #add laplacian (the inverse phase covariance of the DMs)
    for i in range(len(nactsCumList)-1):
        res[nactsCumList[i]:nactsCumList[i+1],nactsCumList[i]:nactsCumList[i+1]]+=laplist[i]*(scale2/scale)

    #Now the inversion:
    t1=time.time()
    inv=r.makeLUInv(res,save=0)
    print "Inversion took %g s"%(time.time()-t1)

    #and make rmx
    #rmxNgs=r.denseDotDense(transA=1,diagScaling=invNoiseCovNgs,save=0)
    for i in xrange(ngspmx.shape[1]):
        ngspmx[:,i]*=invNoiseCovNgs[i]
    rmxNgs=r.dot(inv,ngspmx).T

    if saveIntermediate:
        print "Saving LO rmx"
        util.FITS.Write(rmx,outputName[:-5]+"LO.fits")

    #os.unlink(r.invname)
    #os.unlink("ngspmx.fits")
    #and average the TT, and add to full rmx.
    pos=0
    for i in range(len(ncentNgsList)):
        nx=ncentNgsList[i]
        colsum=rmxNgs[pos:pos+nx].sum(0)/nx#take from NGS rmx
        rmx[pos:pos+nx]+=colsum#add to Global rmx
        colsum=rmxNgs[pos+ncentsNgs/2:pos+ncentsNgs/2+nx].sum(0)/nx
        rmx[pos+nsubaps:pos+nsubaps+nx]+=colsum
        pos+=nx
    print rmx.shape

#scale from arcsec back to pixels (a multiplication, because thats what you'd have to do to the slope measurements to convert them to arcsec)
s=0
for i in range(len(gsList)):
    gs=gsList[i]
    e=s+ncentList[i]
    rmx[s:e]*=pxlScale[i]
    rmx[s+nsubaps:e+nsubaps]*=pxlScale[i]
    s=e

#if autoscale:
#    outputName="rmx_auto.fits"
#else:
#    outputName="rmx_%g_%g.fits"%(scale,scale2)
#if theoreticalNoiseCov:
#    outputName=outputName[:-5]+"_theo.fits"
if theoreticalNoiseCov:
    header.append("THEONCOV= 1 / theoretical noise covariance used")
if subtractTT==0:
    header.append("subTT   = 0")
util.FITS.Write(rmx.T,outputName,doByteSwap=0,extraHeader=header)
print "Written %s"%outputName

print "Time taken: %g s"%(time.time()-tstart)

print "POLC matrix also required..."
