#dasp, the Durham Adaptive optics Simulation Platform.
#Copyright (C) 2004-2016 Alastair Basden and Durham University.

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.

#You should have received a copy of the GNU Affero General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module solarAnn.py: takes a solar image, and lets an ANN compute the slopes.
"""
import time
import numpy
import base.aobase



class SolarAnn(base.aobase.aobase):
    """Class for doing ANN stuff - takes an input image, and outputs the
    slope measurements.
    """
    def __init__(self,parent,config,args={},forGUISetup=0,debug=None,idstr=None):
        """Initialise the object.  The parent object here should be a dictionary with values for each atmospheric layer, ie instances of iscrn, or DMs which are sending their full surface.  """
        base.aobase.aobase.__init__(self,parent,config,args,forGUISetup=forGUISetup,debug=debug,idstr=idstr)
        self.obj=self.config.getVal("wfsOverview",raiseerror=0)
        wfsobj=self.obj.getWfsByID(idstr)
        self.nsubx=wfsobj.nsubx
        self.nimg=wfsobj.nimg
        self.nFieldX=self.config.getVal("nFieldX")
        self.npxl=(self.nFieldX-1)*self.nimg//2+self.nimg
        if forGUISetup==1:#only for the GUI (to allow MPI modules to get the size of the expected output).
            self.outputData=[(self.nsubx,self.nsubx,2),numpy.float32]
        else:
            self.outputData=numpy.zeros((self.nsubx,self.nsubx,2),numpy.float32)
        

    def generateNext(self,msg=None):
        """
        This function is called when it is okay to produce the next iteration
        of the simulation.
        Not expecting any msgs.
        """
        t1=time.time()
        if self.debug:
            print "solarAnn: GenerateNext (debug=%s)"%str(self.debug)
        self.dataValid=0
        if self.generate==1:
            if self.newDataWaiting:
                if self.parent.dataValid==1:
                    self.inputData=self.parent.outputData
                    self.dataValid=1
                else:
                    print "solarAnn: Waiting for data from wfs camera, but not valid"
        if self.dataValid:
            self.doAnnCalc()

    def doAnnCalc(self):
        """Note - self.inputData.shape will be (nsubx,nsubx,npxl,npxl)
        or (nsubx*npxl,nsubx*npxl) - test before using.
        npxl is equal to (nFieldX-1)*nimg/2+nimg
        """
        print "Please implement something here."
        print "InputData.shape = %s"%str(self.inputData.shape)
        print "outputData.shape = %s"%str(self.outputData.shape)
        

