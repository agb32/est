"""Tests the new iscrn module.  
Note - this may not be kept up to date with the module."""
import numpy
import science.iscrn
import science.wideField
import science.wfscent
import compareSHS
import science.iatmos
import base.readConfig
import base.saveOutput
import util.Ctrl
ctrl=util.Ctrl.Ctrl(globals=globals())
ctrl.doInitialOpenLoop(startiter=0)
ctrl.initialCommand("wf.control['cal_source']=1",freq=-1,startiter=0)
ctrl.initialCommand("wf.control['cal_source']=0",freq=-1,startiter=1)
ctrl.initialCommand("c.newCorrRef(doThreshold=1);print 'Done new corr ref'",freq=-1,startiter=1)

iscrn=science.iscrn.iscrn(None,ctrl.config,idstr="allLayers")
iatmos=science.iatmos.iatmos({"allLayers":iscrn},ctrl.config,idstr="b")
wf=science.wideField.WideField({"allLayers":iscrn},ctrl.config,idstr="a")
c=science.wfscent.wfscent(wf,ctrl.config,idstr="acent")#this does the centroiding
shs=science.wfscent.wfscent(iatmos,ctrl.config,idstr="1")#shs version.

comp=compareSHS.Compare({"nf":shs,"wf":c},ctrl.config)

execOrder=[iscrn,iatmos,wf,c,shs,comp]

if ctrl.config.getVal("saveData",default=0):
    #save the solar images
    save=base.saveOutput.saveOutput(wf,ctrl.config,idstr="solar")
    #save the slopes
    save2=base.saveOutput.saveOutput(c,ctrl.config,idstr="corrslopes")
    save3=base.saveOutput.saveOutput(shs,ctrl.config,idstr="slopes")
    execOrder+=[save,save2,save3]
ctrl.mainloop(execOrder)

